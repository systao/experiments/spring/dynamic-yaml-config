package com.systao.experiments.spring.yaml.greetings;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Accessors(chain=true)
public class GreetingsService {
	@Setter
	@Getter
	private GreetingsConfig config;
	
	public String greetings(String name) {
		String result = String.format(
				"Hello %1$s", 
				name == null ? config.getDefaultParam() : name);
		log.info(result);
		return result;
	}
}
