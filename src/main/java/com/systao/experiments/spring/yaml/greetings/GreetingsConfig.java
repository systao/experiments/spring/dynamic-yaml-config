package com.systao.experiments.spring.yaml.greetings;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GreetingsConfig {
	private String defaultParam;
}
