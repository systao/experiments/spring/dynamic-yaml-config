package com.systao.experiments.spring.yaml;

import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.YamlProcessor.ResolutionMethod;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.source.ConfigurationPropertySource;
import org.springframework.boot.context.properties.source.MapConfigurationPropertySource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.HandlerMapping;

import com.systao.experiments.spring.yaml.greetings.GreetingsConfig;
import com.systao.experiments.spring.yaml.greetings.GreetingsService;

@Configuration
public class ApplicationConfiguration {
	@Autowired 
	private HttpServletRequest request;
	
	@Bean
	@RequestScope
	public GreetingsService greetingsService() {
		@SuppressWarnings("unchecked")
		Map<String, ?> pathVariables = (Map<String, ?>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE); 
		String context = (String) pathVariables.get("context");

		Properties properties = loadProperties(context);

	    ConfigurationPropertySource source = new MapConfigurationPropertySource(properties);
        
        Binder binder = new Binder(source);
        
        GreetingsConfig config = binder.bind("services.greetings", GreetingsConfig.class).get();

        return new GreetingsService().setConfig(config);
	}

	private Properties loadProperties(String context) {
		YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
		factory.setResolutionMethod(ResolutionMethod.OVERRIDE);
		String resource = String.format("/contexts/%1$s.yml", context);
		factory.setResources(new ClassPathResource(resource));
	    factory.setSingleton(true);
	    factory.afterPropertiesSet();
		return factory.getObject();
	}
}
