package com.systao.experiments.spring.yaml.greetings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/greetings")
public class GreetingsController {
	@Autowired
	private GreetingsService service;
	
	@GetMapping(path="/{context}")
	public String greetings(
			@PathVariable(name="context") String context, 
			@RequestParam(name="name", required=false) String name) {
		return service.greetings(name);
	}
}
