# [Pattern] Dynamic context switching in Spring

## Context 

We consider a SaaS/ Web context

## Problem

Component configuration is dynamic and depends on a specific request parameter. 

## Solution

1. Externalize the different configurations (one yaml file per possible configuration)
2. Configure the component as a @RequestScope @Bean 
   1. Retrieve the context. For instance:
      ```
      Map<String, ?> pathVariables = (Map<String, ?>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE); 
	  String context = (String) pathVariables.get("context");
	  ```
   2. Load the configuration using YamlPropertiesFactoryBean
   
   3. Bind the configuration using o.s.boot.context.properties.bind.Binder
   
   4. Instantiate the component and programmatically wire the config 
         
## Code sample

See src/main/java
